"""Tests of the main functionality."""

import pathlib

import pandas as pd
import pytest

import ex2mag

TESTPATH = pathlib.Path(__file__).parent.resolve()
TESTDATAFILE = TESTPATH / "test_data" / "raw_data.xlsx"

# PACKAGEPATH = TESTPATH.parent.resolve()
# sys.path.append(PACKAGEPATH)


@pytest.fixture(scope="function")
def standard_col_headers():
    """Column headers for the standard/clean extension data set."""
    return {
        "stressor": ["Name", "Compartment"],
        "region": ["Country"],
        "sector": ["Sector"],
        "time": ["Year"],
        "value": ["Amount"],
        "unit": ["Unit"],
        "provenance": ["Provenance"],
    }


@pytest.fixture(scope="function")
def dirty_col_headers():
    """Column headers for the dirty extension data set."""
    return {
        "stressor": ["Name", "comp"],
        "region": ["Country"],
        "sector": ["sect."],
        "time": ["Year"],
        "value": ["Amount"],
        "unit": ["Unit"],
        "provenance": ["Comments"],
    }


@pytest.fixture(scope="function")
def clean_ind_ext():
    """Read the clean extension data set from the test_data folder."""
    return pd.read_excel(TESTDATAFILE, sheet_name="clean_ind")


@pytest.fixture(scope="function")
def dirty_ind_ext():
    """Read the dirty extension data set from the test_data folder."""
    return pd.read_excel(TESTDATAFILE, sheet_name="dirty_ind")


def test_identify_col_structure_clean(clean_ind_ext, standard_col_headers):
    """Testing if we can guess the correct structure from a nice data set."""
    assert clean_ind_ext.ext.validate()
    for key, item in clean_ind_ext.ext.structure.__dict__.items():
        if key[0] == "_":
            continue
        assert len(set(standard_col_headers[key]).difference(set(item))) == 0


def test_identify_col_structure_dirty(
    dirty_ind_ext, standard_col_headers, dirty_col_headers
):
    """Testing manual col structure estimation with case sensitivity."""
    # First, check if the dirty data set raised ValidationError
    with pytest.raises(ex2mag.ValidationError):
        dirty_ind_ext.ext.validate()
    # This should work with the default case sensitivity
    assert dirty_ind_ext.ext.structure.region == standard_col_headers["region"]
    # And this should not be in with the standard setting
    assert dirty_ind_ext.ext.structure.sector == []

    # Testing case sensitivity
    dirty_ind_ext.ext.identify_col_structure(case_sensitive=True)
    assert dirty_ind_ext.ext.structure.region == []

    # And testing all column headers
    dirty_ind_ext.ext.identify_col_structure(
        possible_names=dirty_col_headers, case_sensitive=True
    )
    assert dirty_ind_ext.ext.validate() == True


def test_replace_string_nan(clean_ind_ext):
    """Test if replacing nan work."""
    df = clean_ind_ext.copy()
    assert True == df.select_dtypes(include=["object"]).isna().any().any()
    df.ext.replace_nan_in_string_columns()
    assert False == df.select_dtypes(include=["object"]).isna().any().any()


def test_duplicated_clean(clean_ind_ext):
    """Double check if the clean test set is really clean."""
    assert clean_ind_ext.ext.duplicated().any() == False


def test_integration_cleaning_up_ind_ext(dirty_ind_ext):
    """Integration test for cleaning up the dirty industry extension data set."""
    # First, check if the dirty data set raised ValidationError
    with pytest.raises(ex2mag.ValidationError):
        dirty_ind_ext.ext.validate()

    # Clean the column headers
    dirty_ind_ext = dirty_ind_ext.rename(
        columns={"sect.": "Sector", "comp": "Compartment"}
    )
    dirty_ind_ext.loc[:, "Provenance"] = ""
    assert dirty_ind_ext.ext.validate()

    # Then deal with duplicates
    assert len(dirty_ind_ext[dirty_ind_ext.ext.duplicated()]) == 8
    assert len(dirty_ind_ext[dirty_ind_ext.ext.duplicated(keep="last")]) == 4

    # d = dirty_ind_ext
    # import pdb; pdb.set_trace() # DEBUG


def test_string_search(clean_ind_ext):
    """Search string/regex in the whole dataframe."""
    pd.testing.assert_frame_equal(
        clean_ind_ext.ext.search("green"),
        clean_ind_ext[clean_ind_ext.Compartment == "green"],
    )

    pd.testing.assert_frame_equal(
        clean_ind_ext.ext.search("green|blue").sort_values(["Name", "Compartment"]),
        pd.concat(
            [
                clean_ind_ext[clean_ind_ext.Compartment == "green"],
                clean_ind_ext[clean_ind_ext.Compartment == "blue"],
            ],
            axis=0,
        ).sort_values(["Name", "Compartment"]),
    )
