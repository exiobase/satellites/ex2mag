# Case study - Invasive species

This tutorial is based on workflow of establishing an invasive species extension for the EXIOBASE MRIO.

The final extension can be found at TODO link.

## Data sources

The raw data for the extension is based on TODO URL and PAPER JAN

## Establish the working folder

We use the [EXIOBASE extension template](https://gitlab.com/exiobase/satellites/extension_template) to establish the working folder for the extension.

For that, we need the [cookiecutter package](https://github.com/cookiecutter/cookiecutter).

We can install cookiecutter either with conda

```bash
conda install -c conda-forge cookiecutter
```

or pip

```bash
pip install cookiecutter
```


Once we have that, we can run the cookiecutter command to create the working folder based on the EXIOBASE extension template.


```bash
cookiecutter gl:exiobase/satellites/extension_template
```

This asks us a couple of questions regarding the new satellite account and then generate an initial folder structure, a README.md and some first code files to get started.

After that, we have a new folder with the name of the satellite account.

In the folder we find a README.md file with some basic information about the satellite account and a folder structure with the following folders:


```bash
.
+-- raw_data_store          repository for data sent per email or manually downloaded - this is tracked by git
|   |
|   +-- provider_a          downloaded files (e.g. ./raw_data_store/FAO)
|   |   |                   
|   |   +-- desc.md         document manual download date, url, provider (TODO: structured? yaml?)
|   |
|   +-- provider_b          downloaded files (e.g. ./raw_data_store/OECD)
|       |                   
|       +-- desc.md         document manual download date, url, provider (TODO: structured? yaml?)
|
|
+-- data                     all things stored here are not tracked by git - these should all be fully reproducible by the src scripts
|   |
|   +-- auto_down            all automatically downloaded data
|   |   +-- provider_a       (rename to the specific data provider)
|   |   +-- provider_b      
|   |
|   +-- clean_native         cleaning/modifications steps in native classification (can include interpolations/gap filling)
|   |   +-- provider_a      
|   |
|   |
|   +-- e3adj                all data ajustments in exiobase 3 classifiation
|   |   |
|   |   +-- class             original data converted to exiobase3 sector/country classification 
|   |   |
|   |   +-- coeff             exiobase data converted to coefficients (absolute values / monetary output)
|   |   |
|   |   +-- inter             interpolation of missing coefficients over years and regions
|   |   |
|   |   +-- xtra              extrapolations (now-, for- and backcasting)
|   |
|   |
|   +-- e3final               final extension data in exiobase 3 classification
|   |   |
|   |   +-- coeff             coefficient format
|   |   |
|   |   +-- abs               absolute values
|   |
|   +-- e4adj                 all data adjustments in exiobase 4 classifiation
|   |   |
|   |   +-- class             original data converted to exiobase4 sector/country classification 
|   |   |
|   |   +-- coeff             exiobase data converted to coefficients (absolute values / monetary output)
|   |   |
|   |   +-- inter             interpolation of missing coefficients over years and regions
|   |   |
|   |   +-- xtra              extrapolations (now-, for- and backcasting)
|   |
|   |
|   +-- e4final              final extension data in exiobase 4 classification
|       |
|       +-- coeff             coefficient format
|       |
|       +-- abs               absolute values
|
+-- docs                      any extensive documentation to long for the README.md(perfered in markdown)
|
+-- logs                      log files
|
+-- src                       src code modules
|   +-- main.py               main entry point, MUST include a function 'run' 
|   +-- conf.py               configuration and helper functions
|
|
|   Files:
|
+-- environment.yml             all requirments/packages to run - see note below
|
+-- README.md                   this file, should contain the docs (if small) or link to /docs
```

## Storing the raw data

There are two possibilities to store raw data in the repository:

In the `raw_data_store` folder, we store all data that is manually downloaded or sent per email. This data is tracked by git so it is fully reproducible. 
However, we need to take care to (1) not store any data that is not publicly available and (2) not store any data that is too large (e.g. > 100MB).

The better solution is to automatically download the data and store it in the `data/auto_down` folder. This data is not tracked by git and can be as large as needed.

For the invasive species extension, we use the second option and download the data from TODO REPOSITORY. 
Currently we have the data available on the NTNU network drive.

So we will write a small script to get the data

## First script - download the data

All scripts for data processing are in the `src` folder.

In the `src` folder we find a `main.py` file with a `run` function. 
This function needs to be there and is the entry point for the extension.
It will be called from the EXIOBASE data pipeline to (re)build the extension.

In the run.py, logging and configuration is already set up.
The configuration is loaded from the `src/conf.py` file which establish the logging (based on [loguru](https://github.com/Delgan/loguru) and some utility functions based on the modules in  `src/util/admin.py`.

To get started, we will write a small script to download the data from the NTNU network drive.

First, we have to define the data source in the `src/conf.py` file.

Here we call it raw_data_ntnu and define the path to the data on the NTNU network drive.

```python
##  SECTION RAW DATA
raw_data_ntnu = Path('/media/ntnu/Xdrive/indecol/USERS/JanB/Invasive_Spec_Footprint')
```

We can then make a small function to download the data from the network drive to the `data/auto_down` folder.

This can either be in a new python file or we add it to the main.py file (fine for small functions).
We use the latter here, and after adding the relevant parts, main.py should look something like this:

```python

"""
Main entry point for compiling the Invasive Species Extension.

"""

import shutil

import conf

logger = conf.logger


def run():
    """Main function to run to compile the complete extension."""
    logger.info("Start building the Invasive Species Extension data")

    raw_data = get_data_ntnu(conf.raw_data_ntnu, conf.data.auto_down)
    return locals()


def get_data_ntnu(src_file, dst_folder):
    """ Get data from NTNU
    """
    logger.info("Getting data from NTNU")
    result_file = dst_folder / src_file.name
    if not result_file.exists():
        logger.info(f"Copying {src_file} to {result_file}")
        shutil.copy(src_file, result_file)
    else:
        logger.info(f"File {result_file} already exists, skipping copy")
    logger.info("Done getting data from NTNU network drive")
    return dst_folder / src_file.name

if __name__ == "__main__":
    try:
        locals().update(run())
    except Exception as e:
        raise


```

When running this script, we should see the following output:

```bash
❯ python main.py
2023-05-22 13:16:26 | INFO | util.admin:setup_loggers:165 - Main node running on uname_result(system='Linux', node='NTNU09417', release='5.15.0-70-generi
c', version='#77~20.04.1-Ubuntu SMP Wed Apr 5 09:38:34 UTC 2023', machine='x86_64')
2023-05-22 13:16:26 | INFO | util.admin:setup_loggers:166 - Running on Python version 3.11.3
2023-05-22 13:16:26 | INFO | __main__:run:15 - Start building the Invasive Species Extension data
2023-05-22 13:16:26 | INFO | __main__:get_data_ntnu:24 - Getting data from NTNU
2023-05-22 13:16:26 | INFO | __main__:get_data_ntnu:27 - Copying /media/ntnu/Xdrive/indecol/USERS/JanB/Invasive_Spec_Footprint/BACI_HS92_Y2019_V202102_im
pacts.csv to /home/konstans/proj/EXIOBASE/exio_satellite/inv_spec/data/auto_down/BACI_HS92_Y2019_V202102_impacts.csv
2023-05-22 13:16:41 | INFO | __main__:get_data_ntnu:31 - Done getting data from NTNU network drive
```

These are the log messages from the script, which are also stored in the /logs folder.

## Establishing the git repository

With the first script in place, it is time to establish the git repository.

For that, we follow the guidelines in the README.md file created during the template creation.

```bash
# initialize the git repository
git init
git add .
git commit -m "Project init"
```

The extension template already contains a `.gitignore` file, which should be fine for most cases.
It includes all data stored in the `data/` folder, so we do not track any data in the repository which can be regenerated by running the scripts.

We can now create a new repository on github, gitlab or any other git hosting service and push the repository to the remote.

## Second script - process the data

Before we continue, we should first have a short look at the data.

After the downloading script, we should have a file `data/auto_down/BACI_HS92_Y2019_V202102_impacts.csv` which contains the data.
We can have a look at the data using the `head` command (linux or mac) or the `more` command (windows).


```bash
head data/auto_down/BACI_HS92_Y2019_V202102_impacts.csv
```

This should give us the following output:

```csv
"t";"i";"j";"k";"v";"q";"commodity";"importer";"exporter";"kg";"route";"PDFyrs";"PDFyrs_marginal"
2019;4;28;620462;0,483;0,011;"620462";"ATG";"AFG";1,1e-05;"AFGATG";1,1665128884389e-16;4,63529468441632e-19
2019;4;31;70310;5,652;22;"070310";"AZE";"AFG";0,022;"AFGAZE";1,89657242879269e-14;5,28242387067404e-17
2019;4;31;80211;2,412;0,196;"080211";"AZE";"AFG";0,000196;"AFGAZE";1,68967361837894e-16;4,70615944841869e-19
2019;4;31;80620;0,176;0,028;"080620";"AZE";"AFG";2,8e-05;"AFGAZE";2,41381945482706e-17;6,72308492631241e-20
2019;4;31;80710;5,889;37,83;"080710";"AZE";"AFG";0,03783;"AFGAZE";3,2612424991467e-14;9,08336795579995e-17
```

So we have some data on trade flows between countries, which we can use to calculate the footprint of the trade flows.
We can also see that a semi-colon is used as a separator, and that the decimal separator is a comma.

From the author of the dataset we also got the follwing information:

- t is time
- i is exporter j j importer (BACI system)
- k is BACI product
- v 1000 current USD 
- q metrictons 
- commodities in hs92
- PDF and PDFmarginal in pdf per kg

We can now create a second script to process the data.

We create a new file `process_data.py` in the `src` folder, and add the following code:

```python
"""
Process the data for the Invasive Species Extension.

"""

import pandas as pd

import conf

logger = conf.logger

raw = pd.read_csv(conf.data.auto_down / "BACI_HS92_Y2019_V202102_impacts.csv", sep=";", decimal=",")



```


## Background

To connect to the EE-MRIO EXIOBASE we also incorporate a demand pull /
consumption based accounting model for the attribution of invasive species
impacts. In short, we attribute the territorial/production based invasive
species impact to the product which demands the imports for each production
structure or the final demand which is satisfied by direct imports..

In detail, we use the following steps:

1. Convert to EXIOBASE classification

Here we aggregate the BACI level invasive species impact data to the EXIOBASE
classification. This includes a mapping of the BACI (HS) product classification
to the EXIOBASE classification as well as a country aggregation. EXIOBASE
models 43 countries explicitly, so these could just be mapped. The rest were
aggregated into the 5 (Rest of the World) RoW regions of EXIOBASE. Note, that
the latter implies that invasive species impacts can occur via pseudo-domestic
flows within the same RoW region.

2. Distribution of invasive species impacts to the EXIOBASE supply chain

The aggregated invasive species impacts from step 1 provides information of
species impacts to do trade (exporter-importer) per product. Here we distribute
this impact to the products which use the imports in their production
structure.

To do so, we extract production structure for each country/region in EXIOBASE.
We then calculated import shares per product (over the combined monetary flow,
Z, and finald demand, Y, matrix).

This is then used to distribute the single value of invasive species impact per
product to the products which use the imports in their production structure as
well as direct final demand imports.

3. Establish novel EXIOBASE satellite account

With the data of step 2, we can now establish a novel satellite account for
EXIOBASE for the year given of the BACI data (2019). This consists of just
summing the impacts per product and finald demand category for each
country/region. This gives us the total invasive species impact per
product/region/final demand, assigned to the product/final demand which
demanded the imports.

4. Calculate the invasive species footprint

The extension data of step 3 provide territorial/production based invasive
species impacts. However, in the globalised economoy a substantial share of
production is due to satisfy final demand in other countries. To account for
this, we use the EXIOBASE MRIO model to calculate the invasive species
footprint. This allows us to track the invasive species impacts occuring a
country/region to the final demand of other countries/regions. Thus while
impact might occur in country A (the invasive species is imported in country A
to allow production of product X), the footprint might be in country B (the
product X is exported to country B to satisfy final demand in country B).

To do so we use standard consumption based accounting as implemented in the
pymrio package.
https://openresearchsoftware.metajnl.com/articles/10.5334/jors.251

The article also describes the standard formulas and aggregation routines used
for the final results.





