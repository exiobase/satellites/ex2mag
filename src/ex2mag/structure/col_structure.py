"""Definitions, standard values and errors."""

from dataclasses import InitVar, dataclass

# Potential column names (case insensitive)
STANDARD_COL_NAMES = {
    "stressor": ["name", "compartment", "stressor", "PhysicalTypeName"],
    "region": ["country", "region", "CountryCode"],
    "sector": ["sector", "product", "industry", "category", "commodity", "ProductTypeCode"],
    "time": ["year", "time", "AccountingYear"],
    "value": ["amount", "value"],
    "unit": ["unit", "UnitCode"],
    "provenance": ["provenance", "history", "log"],
}


@dataclass
class ExtColStructure:
    """Define the structure of the extension dataframe.

    Can be init with just structure = ExtColStructure(stressor='abc', ...) for
    all fields.

    The usual case would be to run

    structure = ExtColStructure(ext_columns=df_ext.columns,
    possible_names=STANDARD_COL_NAMES)

    where ext_columns are all columns present in the dataframe and possible
    names is a dict with keys same as the fields in ExtColStructure and values
    are list of possible values (see STANDARD_COL_NAMES for an example).

    Both approaches can be combined to pre-define some specific fields.

    The field _required_unique contains a tuple of names of attributes which
    need to be unqiue in a extension data set.

    Parameters
    ----------
    pot_names : dict
        A dictionary with the possible names for each field (stressor,
        region, sector, time, value, unit, provenance). By default names
        are case-insensitve. Check the STANDARD_COL_NAMES for an example

    case_sensitive: boolean, optional
        If the column names are case sensitive. Default is False.



    """

    stressor: list = None
    region: list = None
    sector: list = None
    time: list = None
    value: list = None
    unit: list = None
    provenance: list = None

    _required_unique: tuple = ("stressor", "region", "sector", "time")
    ext_columns: InitVar[list] = None
    possible_names: InitVar[dict] = None
    case_sensitive: InitVar[bool] = False

    def __post_init__(
        self,
        dfcols=ext_columns,
        pot_names=possible_names,
        case_sensitive=case_sensitive,
    ):
        """After init determine the present column names."""
        if case_sensitive:
            for key in self.__dict__.keys():
                if key[0] != "_" and self.__dict__[key] in (None, []):
                    # don`t consider _requ. and only assign if not passed as parameter
                    self.__dict__[key] = [
                        col for col in dfcols if col in pot_names[key]
                    ]
        else:
            for key in self.__dict__.keys():
                if key[0] != "_" and self.__dict__[key] in (None, []):
                    # don`t consider _req. and only assign if not passed as parameter
                    names_to_check = [kk.lower() for kk in pot_names[key]]
                    self.__dict__[key] = [
                        col for col in dfcols if col.lower() in names_to_check
                    ]

    def get_col_names(self, *args):
        """Return column names for the passed categories.

        Passes silently over categories not in structure.

        If no argument is passed all defined columns names are returned.

        To get the required unique column names do
        get_col_names(*structure._required_unique)

        """
        if len(args) == 0:
            args = [aa for aa in self.__dict__.keys() if aa[0] != "_"]

        cols = []
        for cat_arg in args:
            cols = cols + self.__dict__.get(cat_arg, [])
        return cols

    def __setattr__(self, attr, value):
        """Ensure that we have list in all fields, also when passing strings."""
        if type(value) is str:
            value = [value]
        elif type(value) is bool:
            value = value
        elif value is None:
            value = []
        else:
            value = list(value)
        super().__setattr__(attr, value)
