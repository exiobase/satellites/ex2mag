"""Public exceptions, warnings and errors."""


class DuplicationError(Exception):
    """Raised when duplicates disturbing the data flow are present."""

    pass


class ValidationError(Exception):
    """Raised when a validation fails of the dataframe extension fails."""

    pass


class SpecMissingError(Exception):
    """Raised when a required specification is missing."""

    pass
