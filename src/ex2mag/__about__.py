# SPDX-FileCopyrightText: 2023-present Konstantin Stadler <konstantin.stadler@ntnu.no>
#
# SPDX-License-Identifier: MIT
__version__ = "0.0.1"
