"""Exsalator - the Python toolkit for Exiobase extensions.

All functions here work with pandas dataframes in a tidy (long) format.


Things to explain but not to implement: d.sort_values(d.ext.structure.stressor
+ d.ext.structure.region + d.ext.structure.time)
# d.insert(loc=1, column='nname',value=x)

"""

# Next steps:
# TODO FIX STRUCTURE OF IMPORT, MAKE TESTS AND INDIVUDAL RUN
# https://iq-inc.com/importerror-attempted-relative-import/
# TODO: EXTEND: fix units with pint - after material run
# TODO: move duplicates from archive - after material run
# TODO: check any old stuff from archive and then delete - after material run


import pandas as pd

from ex2mag.errors import DuplicationError, SpecMissingError, ValidationError
from ex2mag.structure.col_structure import STANDARD_COL_NAMES, ExtColStructure


# Canonical name + some shortcuts for fewer keystokes
@pd.api.extensions.register_dataframe_accessor("extension")
@pd.api.extensions.register_dataframe_accessor("ext")
@pd.api.extensions.register_dataframe_accessor("e")
class Ex2MagAccessor:
    """Accessor for Exiobase extensions."""

    @staticmethod
    def _make_query_from_dict(query_dict):
        """Query string from a dict."""
        dd = []
        for key, item in query_dict.items():
            qs = f"{key} == '{item}'"
            dd.append(qs)
        return " and ".join(dd)

    @staticmethod
    def __make_query(df):
        """Query from a dataframe."""
        # TODO continue here for quereies
        # Missing cases: empty/nan
        # as list for quereies

        def ff(cc):
            # s = f"{cc.index[0]} == {cc}"
            a = []
            for nr, x in enumerate(cc):
                s = f"{cc.index[nr]} == {x}"
                a.append(s)
            aa = " and ".join(a)
            return aa
            # return cc.Name + str(cc.Compartment)

        df.apply(func=ff, axis=1)

        df.apply(func=lambda x: " and ".join(str(x)), axis=0)
        # evstr = " + ' and ' + ".join(col_to_query)
        # evstr = 'query = ' + evstr
        # df = df.eval(evstr, engine='python')

    def __init__(self, pandas_obj):
        """Initialize the accessor.

        TODO DOCUMENT:
        - .structure must be list type (not string).
        """
        self._df = pandas_obj
        self.identify_col_structure(possible_names=STANDARD_COL_NAMES)

    def identify_col_structure(
        self, possible_names=STANDARD_COL_NAMES, case_sensitive=False
    ):
        """Determine the meaning of the column headers.

        Parameters
        ----------
        possible_names: dict, optional
            A dictionary with the possible names for each field (stressor,
            region, sector, time, value, unit, provenance). By default, names
            are case-insensitve. Check the STANDARD_COL_NAMES for an example

        case_sensitive: boolean, optional
           If the matching of column names to possible names is case sensitive.
           Default is False.

        Returns
        -------
            dataclass
                Estimated structure of the extension account. This is also set
                to df.extension.structure
        """
        col_struct = ExtColStructure(
            ext_columns=self._df.columns,
            possible_names=possible_names,
            case_sensitive=case_sensitive,
        )
        self.structure = col_struct
        return col_struct

    def validate(self):
        """Validate the dataframe for confirmation with the expected structure.

        Deviations from the expected structure are raised as ValidationError.
        If everything is ok, returns True.

        see also .stat (TODO)

        Returns
        -------
        Boolean (True if everything is ok)

        Raises
        ------
        ValidationError (if Validation fails)

        """
        # TODO must also change for correct dtype across columns
        error_collector = []
        for key, item in self.structure.__dict__.items():
            if key[0] == "_":
                continue
            if len(item) == 0:
                error_collector.append(f"Required field >{key}< not defined")
                continue
            if type(item) not in (list, tuple):
                error_collector.append(
                    f"Entry in field >{key}< is {type(item)} - must be list."
                )
                continue

            for col_name in item:
                if col_name not in self._df.columns:
                    error_collector.append(
                        f"Field >{key}< contains name >{col_name}< "
                        " which is not present in the dataframe columns"
                    )
        if error_collector:
            raise ValidationError("\n".join(error_collector))
        else:
            return True

    def merge_str_col(self, merge_columns, sep=" | ", strip_trailing_sep=True):
        """Merge columns.

        Note
        ----
        To expand the merged columns use
        df[[names_list]] = df[merged_col].str.split(sep, expand=True)


        Parameters
        ----------
        merge_columns: list
            List of columns to merge.

        sep: string
            String to separate the names (including whitespace), default: ' | '

        strip_trailing_sep: bool, optional
            Remove leading/trailing separators in case the entry in the
            first/last column to merge was empty/nan

        Returns
        -------
            pd.DataFrame
                with merged columns

        """
        df = self._df.loc[:, merge_columns].fillna("")

        new_col = df.loc[:, merge_columns].agg(sep.join, axis=1).str.strip()

        if strip_trailing_sep:
            new_col = new_col.str.strip(sep).str.strip()

        return new_col

    def strip_whitespace(self):
        """Remove leading/trainling whitespace in object type columns (inplace)."""
        for col in self._df.columns:
            if pd.api.types.is_string_dtype(
                self._df[col]
            ) or pd.api.types.is_object_dtype(self._df[col]):
                self._df[col] = self._df[col].str.strip()

    def replace_nan_in_string_columns(self):
        """Replace nan values by '' in object type columns (inplace)."""
        for col in self._df.columns:
            # if pd.api.types.is_string_dtype(self._df[col]):
            if pd.api.types.is_string_dtype(
                self._df[col]
            ) or pd.api.types.is_object_dtype(self._df[col]):
                self._df[col] = self._df[col].fillna("")

    def get_counts_for(self, entry):
        """Get value counts for entry.

        Parameters
        ----------
        entry: str, list (valid column header)
            This can be just the name of one or more columns, or one can pass
            df.ext.structure.XXX To combine several structure field use '+',
            e.g. get_counts_for(df.ext.structure.sector +
            d.ext.structure.region)

        Returns
        -------
        pd.DataFrame
            Unique entries + value count

        Raises
        ------
        SpecMissingError in case of missing specificaation

        """
        try:
            grp = self._df.groupby(entry, dropna=False)
            values = grp[self.structure.value].describe()
            return values
        except (ValueError, KeyError) as err:
            raise SpecMissingError(f"Column {entry} not defined.") from err

    @property
    def stats(self):
        """Statistic and description of the extension."""
        return self.get_counts_for(
            self.structure.region
            + self.structure.stressor
            + self.structure.sector
            + self.structure.unit
        )

    @property
    def regions(self):
        """All available regions."""
        return self.get_counts_for(self.structure.region)

    @property
    def stressors(self):
        """Get the available stressors as list of dict."""
        return self.get_counts_for(self.structure.stressor)

    @property
    def times(self):
        """All available time points."""
        return self.get_counts_for(self.structure.time)

    @property
    def sectors(self):
        """All available sectors."""
        return self.get_counts_for(self.structure.sector)

    @property
    def units(self):
        """All available units."""
        return self.get_counts_for(self.structure.unit)

    def __make_query_for(self, entry):
        """Get the available stressors as list of dict.

        The individual list entries can be used as query for df.query.

        Note
        ----

        Remember to remove NaN in string columns first
        (df.ext.replace_nan_in_string_columns())!

        Parameters
        ----------
        entry: string
            For which structure entry (from df.ext.structure) the query should
            be prepared
        """
        return [
            self._make_query_from_dict(x)
            for x in self._df[self.structure.get_col_names(entry)]
            .drop_duplicates()
            .to_dict("records")
        ]

    def duplicated(
        self,
        categories: list | None = None,
        subset: list | None = None,
        keep: str | bool = False,
        raise_exception: bool = False,
    ):
        """Check for duplicates in the extension.

        This is just a wrapper/extension around pd.duplicated.

        Parameters as in
        https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.duplicated.html
        with some sane defaults for the extension data.

        Parameters
        ----------
        categories: list, optional
            Which categories within df.ext.structure need to be unqiue. If set
            to None (default) this is set to self.structure._required_unique
            which includes (unless set differently by the user) the categories:
            'stressor', 'region', 'sector', 'time'. Note: this are not the
            actual column names (use subset for this)

        subset : list, optional
            Only available if categories is not passed.
            Column names to consider, similar to pure pandas.duplicated

        keep: str | False, optional
            Which occurence of duplicates to mark. Can be 'first', 'last' or None.
            For None, it marks all occurences.

        raise_exception: bool, optional
            It True raises an exception when duplicates are found.


        Returns
        -------
        Series: Boolean series

        Raises
        ------
        DuplicationError (if duplicates are found and raise_exception set to True)


        """
        if categories:
            subset = self.structure.get_col_names(*categories)

        if (categories is None) and (subset is None):
            subset = self.structure.get_col_names(*self.structure._required_unique)

        duplicates = self._df.duplicated(subset=subset, keep=keep)
        if (dupl := len(duplicates)) > 0 and raise_exception:
            raise DuplicationError(f"{dupl} duplicates in the extension data")
        else:
            return duplicates

    def search(self, search_string):
        """Search within the whole extension for search_strings.

        Internally this uses pd.str.contains, so regex expression are valid
        """
        res = self._df[
            self._df.apply(
                lambda row: row.astype(str).str.contains(search_string).any(), axis=1
            )
        ]
        return res


def develop():
    """Short developing script, not used in production."""
    pd.read_excel("./tests/test_data/raw_data.xlsx", sheet_name="clean_ind")
