# ex2mag

[![PyPI - Version](https://img.shields.io/pypi/v/ex2mag.svg)](https://pypi.org/project/ex2mag)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/ex2mag.svg)](https://pypi.org/project/ex2mag)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install ex2mag
```

## License

`ex2mag` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
